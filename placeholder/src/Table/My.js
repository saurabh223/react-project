import React,{Component} from "react";
import { useState } from "react";
import { useEffect } from "react";
import Table from 'react-bootstrap/Table';
function My() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
  
    // Note: the empty deps array [] means
    // this useEffect will run once
    // similar to componentDidMount()
    useEffect(() => {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);
            setItems(result);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )
    }, [])
  
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Website</th>
          </tr>
        </thead>
        <tbody>
        {items.map(item => (
            <tr key={item.id}>
                <th>{item.name}</th>
              <td>{item.email}</td>
              <td>{item.phone}</td>
              <td>{item.address.city}</td>
              <td>{item.website}</td>
            </tr>
          ))}
        </tbody>
      </Table>
          
      );
    }
  }
  export default My;