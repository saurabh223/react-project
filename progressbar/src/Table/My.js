import React from "react";
import {useState} from "react";
import {useEffect} from "react";
import ProgressBar from "react-bootstrap/esm/ProgressBar";
import Table from 'react-bootstrap/Table';
function My() {
  const [error,
    setError] = useState(null);
  const [isLoaded,
    setIsLoaded] = useState(false);
  const [items,
    setItems] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users").then(res => {
      if (!res.ok) {
        throw Error('could not load data')
      }
      return res.json()
    }).then((result) => {
      setIsLoaded(true);
      setItems(result);

    }, (error) => {
      setIsLoaded(true);
      setError(error);
    })
  }, [])
  useEffect(() => {
    setIsLoaded(true);
    setTimeout(() => {
      setIsLoaded(false);
    }, 5000);
  }, []);

  if (error) {
    return <div style={{
      color: "red",
      fontSize: "36px"
    }}>Error: {error.message}</div>;
  } else if (isLoaded) {
    return <div>
      {isLoaded
        ? (<ProgressBar animated now={60}/>)
        : (
          <h1 style={{
            color: "green"
          }}>Successs</h1>
        )}</div>;
  } else {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Website</th>
          </tr>
        </thead>
        <tbody>
          {items.map(item => (
            <tr key={item.id}>
              <th>{item.name}</th>
              <td>{item.email}</td>
              <td>{item.phone}</td>
              <td>{item.address.city}</td>
              <td>{item.website}</td>
            </tr>
          ))}
        </tbody>
      </Table>

    );
  }
}
export default My;