function Car(props) {
    return <h2>I am a { props.brand}!</h2>;
  }
  
  function Garage() {
    return (
      <>
          <h1>Who lives in my garage?</h1>
          <Car brand="ford" />
          <Car brand="jawa" />
          <Car brand="lambo" />
      </>
      
    );
  }
  export default Garage;