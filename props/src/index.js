import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import Cards from './Props/Card';
import Row from 'react-bootstrap/Row';


ReactDOM.render(
  <React.StrictMode>
    <Row>
<Cards imgsrc="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
title="tree image"
btn="Click for more "/>
<Cards imgsrc="https://technologie-f-mauriac.jimdofree.com/app/download/8664189194/bmp_oiseau003.bmp?t=1395577362"
title="Bird"
btn="Click to view birds"/>
<Cards imgsrc="https://images.unsplash.com/photo-1567157577867-05ccb1388e66?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000&q=80"
title="Nariman Point"
btn="click to view "/></Row>
  </React.StrictMode>,
  
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
